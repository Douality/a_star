#include <iostream>
#include <vector>
#include <map>
#include "Vec3.h"
typedef std::vector<long int> position;
typedef long unsigned int lui;

class mapping{
private:
    position current;
    position goal;
    long int n;
    long int m;
    long int borne_gauche=0;
    long int borne_droite=0;
    long int borne_haut=0;
    long int borne_bas=0;
    long int current_pos_x=0;
    long int current_pos_y=0;
    long int borne_avant=0;
    long int borne_arriere=0;
    std::map<long unsigned int,long unsigned int> obstacles;
public:
     lui get_indice(lui i, lui j){
       return i*this->n+j;
     }
     lui vec_to_pos(position p){
       return p[0]*this->n+p[1];
     }
     position get_pos(lui i, lui j){
       position p;
       p.resize(3);
       p[0]=i;
       p[1]=j;
       p[2]=0;
     }
     void object_info(){
       std::cout<<"0== "<<std::endl;
       std::cout<<"1== "<<std::endl;
       std::cout<<"2==O"<<std::endl;
       std::cout<<"3==R"<<std::endl;
       std::cout<<"4==D"<<std::endl;
       std::cout<<"else==U"<<std::endl;
     }
     std::string object_type(int i){
       if(i==0) return " ";
       if(i==1) return " ";
       if(i==2) return "O";
       if(i==3) return "R";
       if(i==4) return "D";
       else return "U";
     }
     void display(){
       for(lui i=0;i<this->n;i++){
         for(lui j=0;j<this->m;j++){
           std::cout<<"|";
           std::cout<<this->object_type(this->obstacles[this->get_indice(i,j)]);
         }
         std::cout<<"|"<<std::endl;
       }
     }
     mapping(){
       this->current.resize(3);
       this->goal.resize(3);
       position p;
       lui x,y,z;
       std::cout<<" start_position (x,y,z) please"<<std::endl;
       std::cin>>x;
       std::cin>>y;
       std::cin>>z;
       this->current[0]=x;
       this->current[1]=x;
       this->current[2]=x;
       std::cout<<" distributeur(x,y,z) please"<<std::endl;
       std::cin>>x;
       std::cin>>y;
       std::cin>>z;
       this->goal[0]=x;
       this->goal[1]=x;
       this->goal[2]=x;
       std::cout<<"Map n*m please"<<std::endl;
       std::cin>>this->n;
       std::cin>>this->m;
       this->borne_gauche=0;
       this->borne_droite=this->n;
       this->borne_haut=this->m;
       this->borne_bas=0;
       this->current_pos_x=this->current[0];
       this->current_pos_y=this->current[1];
       this->borne_avant=0;
       this->borne_arriere=0;
       int alea=1;
       std::cout<<"Generation aleatoire? 1 oui, 0 non"<<std::endl;
       std::cin>>alea;
       if(alea==1){
         for(lui i=0;i<this->n;i++){
           for(lui j=0;j<this->m;j++){
             this->obstacles[this->get_indice(i,j)]=(lui)(rand()%2+1);
           }
         }
       }
       else{
         for(lui i=0;i<this->n;i++){
           for(lui j=0;j<this->m;j++){
             lui obj;
             this->object_info();
             std::cout<<"? : "<<std::endl;
             std::cin>>obj;
             this->obstacles[this->get_indice(i,j)]=obj;
           }
         }
       }
       this->obstacles[this->vec_to_pos(this->current)]=3;
       this->obstacles[this->vec_to_pos(this->goal)]=4;
       this->display();
       std::cout<<"modif? 1 oui, 0 non"<<std::endl;
       std::cin>>alea;
       while(alea==1){
          std::cout<<"where ? (i,j)"<<std::endl;
          lui obj;
          lui i,j;
          std::cin>>i>>j;
          std::cout<<"for ? "<<std::endl;
          std::cin>>obj;
          this->obstacles[this->get_indice(i,j)]=obj;
          this->display();
          std::cout<<"continue modif? 1 oui, 0 non"<<std::endl;
          std::cin>>alea;
       }
     };
     mapping(position p,position dp){
       this->current=p;
       this->goal=dp;
       std::cout<<"Map n*m please"<<std::endl;
       std::cin>>this->n;
       std::cin>>this->m;
       this->borne_gauche=0;
       this->borne_droite=this->n;
       this->borne_haut=this->m;
       this->borne_bas=0;
       this->current_pos_x=this->current[0];
       this->current_pos_y=this->current[1];
       this->borne_avant=0;
       this->borne_arriere=0;
       int alea=1;
       std::cout<<"Generation aleatoire? 1 oui, 0 non"<<std::endl;
       std::cin>>alea;
       if(alea==1){
         for(lui i=0;i<this->n;i++){
           for(lui j=0;j<this->m;j++){
             this->obstacles[this->get_indice(i,j)]=(lui)(rand()%2+1);
           }
         }
       }
       else{
         for(lui i=0;i<this->n;i++){
           for(lui j=0;j<this->m;j++){
             lui obj;
             this->object_info();
             std::cout<<"? : "<<std::endl;
             std::cin>>obj;
             this->obstacles[this->get_indice(i,j)]=obj;
           }
         }
       }
       this->obstacles[this->vec_to_pos(this->current)]=3;
       this->obstacles[this->vec_to_pos(this->goal)]=4;
       this->display();
       std::cout<<"modif? 1 oui, 0 non"<<std::endl;
       std::cin>>alea;
       while(alea==1){
          std::cout<<"where ? (i,j)"<<std::endl;
          lui obj;
          lui i,j;
          std::cin>>i>>j;
          std::cout<<"for ? "<<std::endl;
          std::cin>>obj;
          this->obstacles[this->get_indice(i,j)]=obj;
          this->display();
          std::cout<<"continue modif? 1 oui, 0 non"<<std::endl;
          std::cin>>alea;
       }
     };
     std::vector<long int> get_borne(){
       std::vector<long int> p;
       p.resize(6);
       p[0]=this->borne_gauche;
       p[1]=this->borne_droite;
       p[2]=this->borne_haut;
       p[3]=this->borne_bas;
       p[4]=this->borne_avant;
       p[5]=this->borne_arriere;
       return p;
     }
     position where(){
       return this->current;
     }
     std::vector<std::vector<long int>> getSuivant(std::vector<long int> pos){
          position borne=this->get_borne();
          position stop=this->goal;// y
          std::vector<std::vector<long int>> resultat;
          long int gp=(pos[0]+1>borne[1]? pos[0]+1 : -1); // x -> 
          long int dp=(pos[0]-1<borne[0]? pos[1]-1 : -1); // <- x
          long int hp=(pos[1]+1<borne[2]? pos[0]+1 : -1); /*     ^
                                                                 |
                                                                 x */
       
          long int bp=(pos[1]-1>borne[3]? pos[1]-1 : -1); /*   x
                                                               |
                                                               v */
          if(gp!=-1 &&   (this->obstacles[vec_to_pos(pos)]==0||this->obstacles[vec_to_pos(pos)==3])){
              std::vector<long int> dpl;
              dpl.resize(pos.size());
              dpl=pos;
              dpl[0]=gp;
              resultat.push_back(dpl);
          }
          if(dp!=-1 &&   (this->obstacles[vec_to_pos(pos)]==0||this->obstacles[vec_to_pos(pos)==3])){ 
              std::vector<long int> dpl;
              dpl.resize(pos.size());
              dpl=pos;
              dpl[0]=dp;
              resultat.push_back(dpl);
          }
          if(hp!=-1 &&   (this->obstacles[vec_to_pos(pos)]==0||this->obstacles[vec_to_pos(pos)==3])){
              std::vector<long int> dpl;
              dpl.resize(pos.size());
              dpl=pos;
              dpl[0]=gp;
              resultat.push_back(dpl);
          }
          if(bp!=-1 &&   (this->obstacles[vec_to_pos(pos)]==0||this->obstacles[vec_to_pos(pos)==3])){
              std::vector<long int> dpl;
              dpl.resize(pos.size());
              dpl=pos;
              dpl[0]=gp;
              resultat.push_back(dpl);
          }
         return resultat;
       }
};