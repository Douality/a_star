#include <iostream>
#include <vector>
typedef long unsigned int lui;
#define CLEAN system("clear");

struct Produit {
  std::string name;
  float code;
  float prix;
  float quantity;

  Produit(std::string _name, float _code, float _prix, float _quantity) {
    name = _name;
    code = _code;
    prix = _prix;
    quantity = _quantity;
  }
};

class Distributeur {
private:
  // 10 cent, 20 cent, 50 cent, 1 euro, 2 euro
  std::vector<int> argent_possible{10, 20, 50, 100, 200};
  float current_argent = 0; // unit is cent
  std::vector<Produit> produits;

public:
  Distributeur() {
    current_argent = 0;
  }

  Distributeur(float _argent, std::vector<Produit> _produits) {
    current_argent = _argent;
    produits = _produits;
  }

  void addArgent(float argent) { current_argent += argent; }

  // add product to distributeur
  void addProduit(Produit p) { produits.push_back(p); }

  // take product out of the distributeur
  void buyProduit(float code) {
    unsigned int produit_index = getProduitIndexByCode(code);
    float produit_prix = produits[produit_index].prix;

    if (current_argent >= produit_prix) {
      // decrease quantity
      produits[produit_index].quantity -= 1;
      // decrease money
      current_argent -= produit_prix;
      //
      std::cout << "Vous achetez 1 " + produits[produit_index].name + "."
                << std::endl;
    } else {
      std::cout << "Monnaie insuffisante." << std::endl;
    }
  }

  // get code of product
  float getProduitCodeByName(std::string product_name) {
    for (unsigned int i = 0; i < produits.size(); i++) {
      if (produits[i].name == product_name) {
        return produits[i].code;
      }
    }
    return -1;
  }

  // get index of product by code
  unsigned int getProduitIndexByCode(float code) {
    for (unsigned int i = 0; i < produits.size(); i++) {
      if (produits[i].code == code) {
        return i;
      }
    }
    return -1;
  }

  // check if a product is available
  bool is_disponible(float code) {
    unsigned int produit_index = getProduitIndexByCode(code);
    if (produit_index != -1) {
      if (produits[produit_index].quantity > 0) {
        return true;
      }
    }
    return false;
  }

  // return argent to customer
  void retournerArgent() {
    for (unsigned int i = argent_possible.size() - 1; i >= 0; i--) {
      int return_argent = (int)current_argent / argent_possible[i];
      int left_argent = (int)current_argent % argent_possible[i];
      //
      std::string coin = "";
      switch (argent_possible[i]) {
      case 10:
        coin = "10 cent";
        break;
      case 20:
        coin = "20 cent";
        break;
      case 50:
        coin = "50 cent";
        break;
      case 100:
        coin = "1 euro";
        break;
      case 200:
        coin = "2 euro";
        break;
      };

      if (return_argent != 0) {
        std::cout << "Distributeur retourne " + std::to_string(return_argent) +
                         " coin " + coin
                  << std::endl;
      }

      // update current money
      current_argent = left_argent;
    }
  }

   void affiche_all(lui modulo,lui ca){
    lui cut=0;
    for(lui i=0;i<this->produits.size();i++){
        std::cout<<"|";
        if(ca==i) {std::cout<<"R";}
        else {std::cout<<"x";}
        cut++;
        if(modulo==cut) {
          std::cout<<"|"<<std::endl;
          cut=0;
          }
         if(i==this->produits.size()-1){
           std::cout<<"|"<<std::endl;
          }
    }
  }

  void affiche_case(lui indice){
    lui espace=0;
    for(lui i=0;i<this->produits[i].name.size();i++) espace++;
    std::cout<<this->produits[indice].name;
    std::cout<<std::endl;
    for(lui i=0;i<espace;i++) std::cout<<"-";
    std::cout<<std::endl;
    std::cout<<"code : "<<this->produits[indice].code;
    std::cout<<std::endl;
    for(lui i=0;i<espace;i++) std::cout<<"-";
    std::cout<<std::endl;
    std::cout<<this->produits[indice].prix<<" centimes";
    std::cout<<std::endl;
    for(lui i=0;i<espace;i++) std::cout<<"-";
    std::cout<<std::endl;
    std::cout<<"Quantite : "<<this->produits[indice].quantity;
    std::cout<<std::endl;
  }

  void affiche_complete(){
    for(lui i=0;i<this->produits.size();i++){
      CLEAN;
      affiche_all(this->produits.size()/4,i);
      affiche_case(i);
    }
  }

  bool affiche_complete_restrict(std::string name){
    float p=getProduitCodeByName(name);
    if(p == -1) {affiche_complete(); return false;}
    else{
    for(lui i=0;i<this->produits.size();i++){
      CLEAN;
      affiche_all(this->produits.size()/2,i);
      std::cout<<std::endl;
      affiche_case(i);
      if(i==p) {
        //retournerArgent()
        return true;
        }
      lui pause;
      std::cout<<"next"<<std::endl;
      std::cin>>pause;
    }
   }
  }
};

