#include <iostream>
#include <vector>
#include <set>
#include <string>
#include <map>
#include "Vec3.h"
#include "mapping.h"
#include "distributeur.h"
typedef std::vector<long int> location;
#define CLEAN system("clear");

class noeud {
public:
  std::vector<long int> position;
  noeud* parent;
  noeud (std::vector<long int> pos, noeud* parent){// constructeur
		this->parent = parent;
		this->position = pos;
	} 
};

class robot{
    private :
        std::vector<long int> position  = std::vector<long int>(3);
				std::vector<long int> destination  = std::vector<long int>(3);
				float argentEnPoche;
				mapping map;
    public :
        int get_x(){return this->position[0];}
				int get_y(){return this->position[1];}
				int get_z(){return this->position[2];}
        float get_argent(){return this->argentEnPoche;}

        robot(){};

        robot(long int a, long int b, long int c, float d, mapping map, std::vector<long int> dest){
            this->position[0] = a;
						this->position[1] = b;
						this->position[2] = c;
            this->argentEnPoche= d;
						this->map = map;
						this->destination = dest;
        };

        std::vector<long int> get_position(){
          return this->position;
        }
        
        double argentDispo(){
            return this->argentEnPoche;
        };		

       noeud* chemin(){
				 std::set<noeud*> ensemble; //ensemble des positions déjà explorées
				 std::vector<noeud*> parcours; //positions à parcourir
				 noeud* p;
				 p->position = this->position;
				 p->parent=NULL;
				 parcours.push_back(p);
				 while(!parcours.empty()){
					noeud* pos = parcours.front();
					 parcours.erase(parcours.begin());
					 if(ensemble.count(pos) == 0){ //si la position actuelle n'as pas été explorée
						 ensemble.insert(pos);
						 if(pos->position[0] == this->destination[0]-1 && pos->position[1] == this->destination[1] && pos->position[2] == this->destination[2]){ //si on se trouve en face du distributeur (à sa gauche sur la map)
							 return pos; //noeud permettant de remonter le chemin entre position de départ et destination
						 }
						 std::vector<std::vector<long int>> nextPos = this->map.getSuivant(pos->position); //on récupère les positions suivantes 
						 for(std::vector<long int> suiv : nextPos ){
							 noeud* suivant;
							 suivant->position = suiv;
							 suivant->parent = pos;
							 parcours.push_back(suivant);
						 }
					 }
				 }
				 return NULL; //NULL car pas de chemin
			 }

		void commander(Distributeur d, std::string boisson){
			d.addArgent(this->argentEnPoche);
			float code = d.getProduitCodeByName(boisson);
      if (code == -1) {
        std::cout << "Le produit n'existe pas'." << std::endl;
      } else {
        if (d.is_disponible(code)) {
          // buy produit
          d.buyProduit(code);
          // get money return
          d.retournerArgent();
        } else {
          std::cout << "Le produit n'est pas disponible." << std::endl;
        }
      }
		}
};