#include "position.h"
class objet{
private:
std::string nom;
float type_objet; //(0,1,2,3,4)
float code;
float prix;
position pos;

public:
objet(float a,float b,float c,float x,float y,float z) {
  this->type_objet=a; //(0,1,2,3,4)
  this->code=b;
  this->prix=c;
  this->pos=position(x,y,z,0);
}
objet(float a,float b,float c,position p) {
  this->type_objet=a; //(0,1,2,3,4)
  this->code=b;
  this->prix=c;
  this->pos=position(p.get_x(),p.get_y(),p.get_z());
}
std::string get_name(){return this->nom;}
float get_object(){return this->type_objet;}
std::string traduction(){
            if(this->get_object()==0) return "rien";
            else if(this->get_object()==1) return "obstacle";
            else if(this->get_object()==2) return "distributeur";
            else if(this->get_object()==3) return "personne";
            else return "robot";
}
float get_code(){return this->code;}
float get_prix(){return this->prix;}
};