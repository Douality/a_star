#define CLEAN system("clear");
#include "robot.h"
#include <iostream>
#include <algorithm>
typedef std::vector<long int> pt;
typedef long unsigned int lui;

int main() {
  //La MACRO CLEAN permet de clear le board dans tous les fichiers, à utiliser pour épurer si nécessaire.
	
  // distributeur
  Distributeur distributeur = Distributeur();
  // init product to distributeur
  // name of product, code, prix en cent, auqntity
  distributeur.addProduit(Produit("coca", 1, 150, 10));
  distributeur.addProduit(Produit("pepsi", 2, 150, 10));
  distributeur.addProduit(Produit("fanta", 3, 150, 10));
  distributeur.addProduit(Produit("eau", 4, 150, 10));
  distributeur.addProduit(Produit("cafe", 5, 150, 10));
  //
  std::vector<long int> p;
    p.resize(3);
    p[0]=1;
    p[1]=1;
    p[2]=1;
    std::vector<long int> d;
    d.resize(3);
    d[0]=5;
    d[1]=5;
    d[2]=5;
  std::cout<<"map 6X6 -> (5,4) -> 0"<<std::endl;
  distributeur.affiche_complete();
  mapping* map=new mapping(p,d,10);
  //CLEAN;
  std::cout<<"deuxième map -> (2,2) -> 0"<<std::endl;
  //robot* rob=new robot(1,1,1,3,*map,d);
	robot* rob = new robot(); //test
  noeud* chemin = rob->chemin();
  std::cout<<"chemin ok"<<std::endl;
  noeud* start=chemin;
  std::vector<std::vector<long int>> chs;
  while(start != NULL){
    chs.push_back(start->position);
    start=start->parent;
  }
  std::reverse(chs.begin(),chs.end());
  std::cout<<"conversion ok"<<std::endl;
  for(std::vector<long int> n : chs){
    map->set_robbot_pos(n);
    map->display();
    lui pause;
    std::cout<<"next"<<std::endl;
    std::cin>>pause;
  }
  std::cout<<"rob est arrive"<<std::endl;
  bool success=distributeur.affiche_complete_restrict("cafe");
  std::reverse(chs.begin(),chs.end());
  std::cout<<"rob va partir"<<std::endl;
  for(std::vector<long int> n : chs){
    map->set_robbot_pos(n);
    map->display();
    lui pause;
    std::cout<<"next"<<std::endl;
    std::cin>>pause;
  }
  std::cout<<"rob est revenu"<<std::endl;
  std::cout<<"End...mission...processing....... "<<success<<"...terminate"<<std::endl;
}
