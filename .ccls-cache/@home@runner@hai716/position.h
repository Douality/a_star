///////////////DISCONTINUED////////////////////////


#include<iostream>
#include<ctime>
#include<chrono>
#include<time.h>
#include<algorithm>
#include<cmath>
#include<vector>
#include<string>
#include<x86intrin.h>

class position{
    private :
        float x;
        float y;
        float z=0;
        float type_dedans;
    public :
        float get_x(){return this->x;}
        float get_y(){return this->x;}
        float get_z(){return this->x;}
        float get_object(){return this->type_dedans;}

        position(){};
        position(float a, float b,float c){
            this->x=a;
            this->y=b;
            this->z=c;
            this->type_dedans=0;
        };
        position(float a, float b,float c,float d){
            this->x=a;
            this->y=b;
            this->z=c;
            this->type_dedans=d;
        };

        position(position const &e){
            this->x=e.get_x();
            this->y=e.get_y();
            this->z=e.get_z();
            this->type_dedans=e->get_object();
        };

        Vec3 get_position(){
          return Vec3(this->x,this->y,this->z);
        }
        
        int dedans(){
            return this->type_dedans;
        };		

        std::string traduction(){
            if(this->get_object()==0) return "rien";
            else if(this->get_object()==1) return "obstacle";
            else if(this->get_object()==2) return "distributeur";
            else if(this->get_object()==3) return "personne";
            else return "robot";
        };		

        std::string image(){
            if(this->get_object()==0) return " ";
            else if(this->get_object()==1) return "x";
            else if(this->get_object()==2) return "D";
            else if(this->get_object()==3) return "p";
            else return "R";
        };		
};
