#include <iostream>
#include <vector>
typedef std::vector<long int> position;

class objet{
private:
std::string nom;
float type_objet; //(0,1,2,3,4)
float code;
float prix;
position pos;

public:
objet(float a,float b,float c,float x,float y,float z) {
  this->type_objet=a; //(0,1,2,3,4)
  this->code=b;
  this->prix=c;
  this->pos.resize(4);
  this->pos[0]=x;
  this->pos[1]=y;
  this->pos[2]=z;
  this->pos[3]=0;
}
objet(float a,float b,float c,std::vector<long int> p) {
  this->type_objet=a; //(0,1,2,3,4)
  this->code=b;
  this->prix=c;
  this->pos[0]=p[0];
  this->pos[1]=p[1];
  this->pos[2]=p[2];
  this->pos[3]=p[3];
}
std::string get_name(){return this->nom;}
float get_object(){return this->type_objet;}
std::string traduction(){
            if(this->get_object()==0) return "rien";
            else if(this->get_object()==1) return "obstacle";
            else if(this->get_object()==2) return "distributeur";
            else if(this->get_object()==3) return "personne";
            else return "robot";
}
float get_code(){return this->code;}
float get_prix(){return this->prix;}
};