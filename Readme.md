# Projet d'Agent de Recherche de Chemin

Ce projet s'incrit dans l'étude Agent et se divise en deux étapes distinctes.
Dans la première étape, nous créons une simulation simple en utilisant un agent qui trouve un chemin pour accomplir une tâche. La deuxième étape implique la création de la même simulation, mais cette fois-ci en utilisant Unreal Engine pour créer un petit jeu.

## Étape 1 : Simulation de Recherche de Chemin

Dans cette première étape, nous utilisons un ensemble de fichiers C++ pour simuler un agent capable de trouver un chemin et accomplir une tâche spécifique. Voici une liste des fichiers inclus dans cette étape :

- `Makefile`
- `Vec3.h`
- `distributeur.h`
- `main`
- `main-debug`
- `main.cpp`
- `main.o`
- `mapping.h`
- `objet.h`
- `replit.nix`
- `robot.h`

### Comment Exécuter l'Étape 1

1. Clonez ce référentiel sur votre ordinateur.
2. Utilisez un environnement de développement compatible avec le langage C++ pour compiler et exécuter les fichiers du projet.
3. Exécuter le main et suivez les instructions, vous contruirez une map sommaire ou vous pourrez redéfinir certains éléments et observer.

## Étape 2 : Implémentation dans Unreal Engine

Dans cette deuxième étape, nous utiliserons Unreal Engine pour créer une version interactive de la simulation. Voici une liste des fichiers inclus dans cette étape :

- `Engine`
- `Samples`
- `Templates`
- `.editorconfig`
- `.gitattributes`
- `.gitignore`
- `GenerateProjectFiles.bat`
- `GenerateProjectFiles.command`
- `GenerateProjectFiles.sh`
- `LICENSE.md`
- `README.md`
- `Setup.bat`
- `Setup.command`
- `Setup.sh`

### Comment Exécuter l'Étape 2

1. Suivez les instructions spécifiques à Unreal Engine pour configurer et exécuter le projet.
2. Utilisez Unreal Engine pour explorer la simulation interactive de l'agent de recherche de chemin.
3. Vous pourrez alors tester le jeu dans un petit style survival
4. Imaginez des ennemis et un nettoyeur, l'objectif et de chercher et eliminer les cibles dans toute la map
