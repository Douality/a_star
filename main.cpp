#define CLEAN system("clear");
#include "robot.h"
#include <iostream>
#include <algorithm>
typedef std::vector<long int> pt;
typedef long unsigned int lui;

std::string boolclean(bool x){
  if(x==true){std::cout<<" oui ";}
  else{std::cout<<" non ";}
  return "";
}
int main() {
  //La MACRO CLEAN permet de clear le board dans tous les fichiers, à utiliser pour épurer si nécessaire.
	
  // distributeur
  Distributeur distributeur = Distributeur(300.0);
  // init product to distributeur
  // name of product, code, prix en cent, auqntity
  distributeur.addProduit(Produit("coca", 1, 150, 10));
  distributeur.addProduit(Produit("pepsi", 2, 150, 10));
  distributeur.addProduit(Produit("fanta", 3, 150, 10));
  distributeur.addProduit(Produit("eau", 4, 150, 10));
  distributeur.addProduit(Produit("cafe", 5, 150, 10));
  std::vector<std::vector<long int>> demo;
  demo.resize(9);
  std::vector<long int> d1,d2,d3,d4,d5,d6,d7,d8,d9;
  d1.resize(2);d2.resize(2);d3.resize(2);d4.resize(2);d5.resize(2);d6.resize(2);d7.resize(2);d8.resize(2);d9.resize(2);
  d1[0]=1;d1[1]=0;
  d2[0]=2;d2[1]=0;
  d3[0]=3;d3[1]=0;
  d4[0]=4;d4[1]=0;
  d5[0]=4;d5[1]=1;
  d6[0]=4;d6[1]=2;
  d7[0]=5;d7[1]=2;
  d8[0]=5;d8[1]=3;
  d9[0]=5;d9[1]=4;
  demo[0]=d1;demo[1]=d2;demo[2]=d3;demo[3]=d4;demo[4]=d5;demo[5]=d6;demo[6]=d7;demo[7]=d8;demo[8]=d9;
  //
  std::vector<long int> p;
    p.resize(3);
    p[0]=1;
    p[1]=1;
    p[2]=1;
    std::vector<long int> d;
    d.resize(3);
    d[0]=5;
    d[1]=5;
    d[2]=5;
  //std::cout<<"Je veux quoi ? (default cafe)"<<std::endl;
  std::string prod="cafe";
  //std::cin>>prod;
  //std::cout<<"map 6X6 -> (5,4) -> 0"<<std::endl;
  mapping* map=new mapping(p,d,10);
  CLEAN;
  map->obstacles[map->get_indice(5,4)]=0;
  map->display();
  std::cout<<std::endl;
  //std::cout<<"Go 0, demo 1"<<std::endl; 
  long unsigned int processing=1;
  bool achete=false;
  //std::cin>>processing;
  if(processing<=0){
    std::cout<<"deuxième map -> (2,2) -> 0"<<std::endl;
    robot* rob=new robot(1,1,1,3,*map,d);
    noeud* chemin = rob->chemin();
    std::cout<<"chemin ok"<<std::endl;
    noeud* start=chemin;
    std::vector<std::vector<long int>> chs;
    while(start != NULL){
      chs.push_back(start->position);
      start=start->parent;
    }
    std::cout<<"................rob a adapte le chemin, let's proceed."<<std::endl;
    std::cout<<std::endl;
    std::reverse(chs.begin(),chs.end());
    char pause;
    for(long unsigned int i=0;i<chs.size();i++){
      map->obstacles[map->get_indice(map->current[0],map->current[1])]=0;
      map->obstacles[map->get_indice(chs[i][0],chs[i][1])]=3;
      map->current=chs[i];
      map->display();
      std::cout<<std::endl;
      std::cout<<"next"<<std::endl;
      std::cin>>pause;
      CLEAN;
    }
    std::cout<<"rob est arrive"<<std::endl;
    bool success=distributeur.affiche_complete_restrict(prod);
    std::cout<<std::endl;
    std::cout<<"rob a trouve...cafe ? "<<boolclean(success)<<std::endl;
    if(success){
      std::cout<<"rob a "<<300.0<<" centimes d'euros"<<std::endl;
      achete=distributeur.buyProduit(distributeur.getProduitCodeByName(prod));
    }
    std::cout<<std::endl;
    std::cout<<" rob a le produit ? "<<boolclean(achete)<<std::endl;
    std::cout<<std::endl;
    std::reverse(chs.begin(),chs.end());
    std::cout<<"rob va partir"<<std::endl;
    std::cout<<std::endl;
    for(long unsigned int i=0;i<chs.size();i++){
      map->obstacles[map->get_indice(map->current[0],map->current[1])]=0;
      map->obstacles[map->get_indice(chs[i][0],chs[i][1])]=3;
      map->current=chs[i];
      map->display();
      std::cout<<std::endl;
      std::cout<<"next"<<std::endl;
      std::cin>>pause;
      CLEAN;
    }
  }
  else{
    std::cout<<"................rob a trouve un chemin, let's proceed."<<std::endl;
    char pause;
    for(long unsigned int i=0;i<demo.size();i++){
      map->obstacles[map->get_indice(map->current[0],map->current[1])]=0;
      map->obstacles[map->get_indice(demo[i][0],demo[i][1])]=3;
      map->current=demo[i];
      map->display();
      std::cout<<std::endl;
      std::cout<<"next"<<std::endl;
      std::cin>>pause;
      CLEAN;
    }
    std::cout<<"rob est arrive"<<std::endl;
    bool success=distributeur.affiche_complete_restrict(prod);
    std::cout<<std::endl;
    std::cout<<"rob a trouve...cafe ? "<<boolclean(success)<<std::endl;
    if(success){
      std::cout<<"rob a "<<300.0<<"centimes"<<std::endl;
      achete=distributeur.buyProduit(distributeur.getProduitCodeByName(prod));
    }
    std::cout<<std::endl;
    std::cout<<" rob a le produit ? "<<boolclean(achete)<<std::endl;
    std::reverse(demo.begin(),demo.end());
    std::cout<<std::endl;
    std::cout<<"rob va partir"<<std::endl;
    std::cout<<std::endl;
    for(long unsigned int i=0;i<demo.size();i++){
      map->obstacles[map->get_indice(map->current[0],map->current[1])]=0;
      map->obstacles[map->get_indice(demo[i][0],demo[i][1])]=3;
      map->current=demo[i];
      map->display();
      std::cout<<std::endl;
      std::cout<<"next"<<std::endl;
      std::cin>>pause;
      CLEAN;
    }
  }
  map->obstacles[map->get_indice(map->current[0],map->current[1])]=0;
  map->obstacles[map->get_indice(p[0],p[1])]=3;
  map->current=p;
  map->display();
  std::cout<<"rob est revenu"<<std::endl;
  std::cout<<"End...mission...processing....... "<<boolclean(achete)<<"...terminate"<<std::endl;
  std::cin>>prod;
  CLEAN;
}
